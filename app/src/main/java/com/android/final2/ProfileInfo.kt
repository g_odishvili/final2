package com.android.final2

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class ProfileInfo : AppCompatActivity() {
    private lateinit var email: TextView
    private lateinit var firstName: TextView
    private lateinit var lastName: TextView
    private lateinit var age: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_info)
        init()
        setData()
    }

    private fun init(){
         email = findViewById(R.id.tv_email)
         firstName = findViewById(R.id.tv_first_name)
         lastName = findViewById(R.id.tv_last_name)
         age= findViewById(R.id.tv_age)
    }

    @SuppressLint("SetTextI18n")
    private fun setData(){
        val serializableExtra: User? = intent.getParcelableExtra("user")
        email.text = email.text.toString() + " " + serializableExtra?.email
        firstName.text = firstName.text.toString() + " "+ serializableExtra?.firstName
        lastName.text = lastName.text.toString() + " "+ serializableExtra?.lasName
        age.text = age.text.toString() + " " + (serializableExtra?.age ?: "").toString()
    }
}