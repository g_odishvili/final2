package com.android.final2

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.final2.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val data = mutableMapOf<String,User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSave.setOnClickListener {
            save()
        }

        binding.btnRemove.setOnClickListener {
            remove()
        }

        binding.btnUpdate.setOnClickListener {
            update()
        }

        binding.btnShowProfile.setOnClickListener {
            profileInfo()
        }
    }

    private fun profileInfo() {
        if (validateForm()) {
            val intent = Intent(this, ProfileInfo::class.java)
            intent.putExtra(
                "user", User(
                    binding.edFirstName.text.toString().trim(),
                        binding.edLastName.text.toString().trim(),
                        binding.edEmail.text.toString().trim(),
                        binding.edAge.text.toString().toInt()
                )
            )
            startActivity(intent);
        }
    }

    private fun update() {
        if (validateForm()) {
            val find = data.contains(binding.edEmail.text.toString().trim())
            if (find) {
                val user =
                    User(
                        binding.edFirstName.text.toString().trim(),
                        binding.edLastName.text.toString().trim(),
                        binding.edEmail.text.toString().trim(),
                        binding.edAge.text.toString().toInt()
                    )
                data[binding.edEmail.text.toString().trim()] = user
                resultPopUp("User Updated Successfully", true)
            } else {
                resultPopUp("User Was Not Found", false)
            }
        }
    }

    private fun remove() {
        if (validateForm()) {
            if (data.remove(binding.edEmail.text.toString().trim()) == null) {
                resultPopUp("User Does not Exists", false)
            } else {
                val s = "Active Users: " + data.size
                binding.tvAdded.text = s
                var substringAfter = binding.tvRemoved.text.toString().substringAfter(" ")
                substringAfter = "Removed: ${substringAfter.toInt() + 1}"
                binding.tvRemoved.text = substringAfter

                resultPopUp("User deleted successfully", true)
            }
        }
    }

    private fun save() {
        if (validateForm()) {
            val find = data.contains(binding.edEmail.text.toString().trim())
            if (!find) {
                val user =
                    User(
                      binding.edFirstName.text.toString().trim(),
                        binding.edLastName.text.toString().trim(),
                        binding.edEmail.text.toString().trim(),
                        binding.edAge.text.toString().toInt()
                    )
                data[binding.edEmail.text.toString().trim()] = user

                val s = "Active Users: " + data.size
                binding.tvAdded.text = s

                resultPopUp("User Added Successfully", true)
            } else {
                resultPopUp("User Already Exists", false)
            }
        }
    }

    private fun validateForm(): Boolean{
        if (binding.edEmail.text.toString().trim().isEmpty() || binding.edFirstName.text.toString().trim().isEmpty() || binding.edLastName.text.toString().trim()
                .isEmpty() || binding.edAge.text.toString().trim().isEmpty()
        ) {
            Toast.makeText(this, getString(R.string.all_fields_filled_error), Toast.LENGTH_SHORT).show()}
        else if (!isValidEmail(binding.edEmail.text.toString())){
            binding.edEmail.error = getString(R.string.valid_email_error)
        }else if ( binding.edAge.text.toString()[0] == '0' ){
            binding.edAge.error = getString(R.string.age_error)
        }else {
            return true
        }
        return false
    }

    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun resultPopUp(message: String,flag: Boolean){
        binding.tvResult.text = message
        binding.tvResult.visibility = View.VISIBLE
        if(flag){
            binding.tvResult.setBackgroundResource(R.color.green)
        }else {
            binding.tvResult.setBackgroundResource(R.color.red)
        }
        val t = Timer(false)

        t.schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {  binding.tvResult.visibility = View.INVISIBLE }
            }
        }, 5000)
    }


}