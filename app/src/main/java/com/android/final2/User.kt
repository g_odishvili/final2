package com.android.final2

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class User(
    val firstName: String,
    val lasName: String,
    val email: String,
    val age: Int,
) : Parcelable
